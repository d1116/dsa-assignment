import ballerina/grpc;

listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "users" on ep {

    remote function add_new_fn(CreateRequest value) returns Response|error {
    }
    remote function add_fns(MultipleRequest value) returns Response|error {
    }
    remote function delete_fn(ShowRequest value) returns Response|error {
    }
    remote function show_fn(ShowRequest value) returns CreateRequest|error {
    }
    remote function show_all_fns(ShowRequest value) returns ShowRepeatedResponse|error {
    }
    remote function show_all_with_criteria(ShowCriteriaRequest value) returns ShowRepeatedResponse|error {
    }
}

