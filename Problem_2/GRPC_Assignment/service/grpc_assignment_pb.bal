import ballerina/grpc;

public isolated client class usersClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(CreateRequest|ContextCreateRequest req) returns (Response|grpc:Error) {
        map<string|string[]> headers = {};
        CreateRequest message;
        if (req is ContextCreateRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <Response>result;
    }

    isolated remote function add_new_fnContext(CreateRequest|ContextCreateRequest req) returns (ContextResponse|grpc:Error) {
        map<string|string[]> headers = {};
        CreateRequest message;
        if (req is ContextCreateRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <Response>result, headers: respHeaders};
    }

    isolated remote function add_fns(MultipleRequest|ContextMultipleRequest req) returns (Response|grpc:Error) {
        map<string|string[]> headers = {};
        MultipleRequest message;
        if (req is ContextMultipleRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/add_fns", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <Response>result;
    }

    isolated remote function add_fnsContext(MultipleRequest|ContextMultipleRequest req) returns (ContextResponse|grpc:Error) {
        map<string|string[]> headers = {};
        MultipleRequest message;
        if (req is ContextMultipleRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/add_fns", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <Response>result, headers: respHeaders};
    }

    isolated remote function delete_fn(ShowRequest|ContextShowRequest req) returns (Response|grpc:Error) {
        map<string|string[]> headers = {};
        ShowRequest message;
        if (req is ContextShowRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <Response>result;
    }

    isolated remote function delete_fnContext(ShowRequest|ContextShowRequest req) returns (ContextResponse|grpc:Error) {
        map<string|string[]> headers = {};
        ShowRequest message;
        if (req is ContextShowRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <Response>result, headers: respHeaders};
    }

    isolated remote function show_fn(ShowRequest|ContextShowRequest req) returns (CreateRequest|grpc:Error) {
        map<string|string[]> headers = {};
        ShowRequest message;
        if (req is ContextShowRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <CreateRequest>result;
    }

    isolated remote function show_fnContext(ShowRequest|ContextShowRequest req) returns (ContextCreateRequest|grpc:Error) {
        map<string|string[]> headers = {};
        ShowRequest message;
        if (req is ContextShowRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <CreateRequest>result, headers: respHeaders};
    }

    isolated remote function show_all_fns(ShowRequest|ContextShowRequest req) returns (ShowRepeatedResponse|grpc:Error) {
        map<string|string[]> headers = {};
        ShowRequest message;
        if (req is ContextShowRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/show_all_fns", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <ShowRepeatedResponse>result;
    }

    isolated remote function show_all_fnsContext(ShowRequest|ContextShowRequest req) returns (ContextShowRepeatedResponse|grpc:Error) {
        map<string|string[]> headers = {};
        ShowRequest message;
        if (req is ContextShowRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/show_all_fns", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <ShowRepeatedResponse>result, headers: respHeaders};
    }

    isolated remote function show_all_with_criteria(ShowCriteriaRequest|ContextShowCriteriaRequest req) returns (ShowRepeatedResponse|grpc:Error) {
        map<string|string[]> headers = {};
        ShowCriteriaRequest message;
        if (req is ContextShowCriteriaRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/show_all_with_criteria", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <ShowRepeatedResponse>result;
    }

    isolated remote function show_all_with_criteriaContext(ShowCriteriaRequest|ContextShowCriteriaRequest req) returns (ContextShowRepeatedResponse|grpc:Error) {
        map<string|string[]> headers = {};
        ShowCriteriaRequest message;
        if (req is ContextShowCriteriaRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("users/show_all_with_criteria", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <ShowRepeatedResponse>result, headers: respHeaders};
    }
}

public client class UsersShowRepeatedResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendShowRepeatedResponse(ShowRepeatedResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextShowRepeatedResponse(ContextShowRepeatedResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class UsersResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendResponse(Response response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextResponse(ContextResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class UsersCreateRequestCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCreateRequest(CreateRequest response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCreateRequest(ContextCreateRequest response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextResponse record {|
    Response content;
    map<string|string[]> headers;
|};

public type ContextMultipleRequest record {|
    MultipleRequest content;
    map<string|string[]> headers;
|};

public type ContextCreateRequest record {|
    CreateRequest content;
    map<string|string[]> headers;
|};

public type ContextShowRequest record {|
    ShowRequest content;
    map<string|string[]> headers;
|};

public type ContextShowRepeatedResponse record {|
    ShowRepeatedResponse content;
    map<string|string[]> headers;
|};

public type ContextShowCriteriaRequest record {|
    ShowCriteriaRequest content;
    map<string|string[]> headers;
|};

public type Response record {|
    string response = "";
|};

public type Function record {|
    string content = "";
    string parameters = "";
|};

public type MultipleRequest record {|
    CreateRequest[] multpleReq = [];
|};

public type ShowResponse record {|
    string firstname = "";
    string lastname = "";
    string email = "";
    int version1 = 0;
    string language = "";
    string function_name = "";
    Function 'function = {};
|};

public type CreateRequest record {|
    string firstname = "";
    string lastname = "";
    string email = "";
    int version1 = 0;
    string language = "";
    string function_name = "";
    Function function1 = {};
|};

public type ShowAllRequest record {|
    int hello = 0;
|};

public type ShowRequest record {|
    string function_name = "";
    int version1 = 0;
|};

public type ShowRepeatedResponse record {|
    ShowResponse[] response = [];
|};

public type ShowCriteriaRequest record {|
    string language = "";
    int version1 = 0;
    string keywords = "";
|};

const string ROOT_DESCRIPTOR = "0A15677270635F61737369676E6D656E742E70726F746F22E5010A0D43726561746552657175657374121C0A0966697273746E616D65180120012809520966697273746E616D65121A0A086C6173746E616D6518022001280952086C6173746E616D6512140A05656D61696C1803200128095205656D61696C121A0A0876657273696F6E31180420012803520876657273696F6E31121A0A086C616E677561676518052001280952086C616E677561676512230A0D66756E6374696F6E5F6E616D65180620012809520C66756E6374696F6E4E616D6512270A0966756E6374696F6E3118072001280B32092E46756E6374696F6E520966756E6374696F6E3122440A0846756E6374696F6E12180A07636F6E74656E741801200128095207636F6E74656E74121E0A0A706172616D6574657273180220012809520A706172616D657465727322260A08526573706F6E7365121A0A08726573706F6E73651801200128095208726573706F6E736522410A0F4D756C7469706C6552657175657374122E0A0A6D756C74706C6552657118012003280B320E2E43726561746552657175657374520A6D756C74706C65526571224E0A0B53686F775265717565737412230A0D66756E6374696F6E5F6E616D65180120012809520C66756E6374696F6E4E616D65121A0A0876657273696F6E31180220012803520876657273696F6E3122260A0E53686F77416C6C5265717565737412140A0568656C6C6F180120012803520568656C6C6F22E2010A0C53686F77526573706F6E7365121C0A0966697273746E616D65180120012809520966697273746E616D65121A0A086C6173746E616D6518022001280952086C6173746E616D6512140A05656D61696C1803200128095205656D61696C121A0A0876657273696F6E31180420012803520876657273696F6E31121A0A086C616E677561676518052001280952086C616E677561676512230A0D66756E6374696F6E5F6E616D65180620012809520C66756E6374696F6E4E616D6512250A0866756E6374696F6E18072001280B32092E46756E6374696F6E520866756E6374696F6E22410A1453686F775265706561746564526573706F6E736512290A08726573706F6E736518012003280B320D2E53686F77526573706F6E73655208726573706F6E736522690A1353686F77437269746572696152657175657374121A0A086C616E677561676518012001280952086C616E6775616765121A0A0876657273696F6E31180220012803520876657273696F6E31121A0A086B6579776F72647318032001280952086B6579776F72647332A3020A05757365727312270A0A6164645F6E65775F666E120E2E437265617465526571756573741A092E526573706F6E736512260A076164645F666E7312102E4D756C7469706C65526571756573741A092E526573706F6E736512240A0964656C6574655F666E120C2E53686F77526571756573741A092E526573706F6E736512270A0773686F775F666E120C2E53686F77526571756573741A0E2E4372656174655265717565737412330A0C73686F775F616C6C5F666E73120C2E53686F77526571756573741A152E53686F775265706561746564526573706F6E736512450A1673686F775F616C6C5F776974685F637269746572696112142E53686F774372697465726961526571756573741A152E53686F775265706561746564526573706F6E7365620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"grpc_assignment.proto": "0A15677270635F61737369676E6D656E742E70726F746F22E5010A0D43726561746552657175657374121C0A0966697273746E616D65180120012809520966697273746E616D65121A0A086C6173746E616D6518022001280952086C6173746E616D6512140A05656D61696C1803200128095205656D61696C121A0A0876657273696F6E31180420012803520876657273696F6E31121A0A086C616E677561676518052001280952086C616E677561676512230A0D66756E6374696F6E5F6E616D65180620012809520C66756E6374696F6E4E616D6512270A0966756E6374696F6E3118072001280B32092E46756E6374696F6E520966756E6374696F6E3122440A0846756E6374696F6E12180A07636F6E74656E741801200128095207636F6E74656E74121E0A0A706172616D6574657273180220012809520A706172616D657465727322260A08526573706F6E7365121A0A08726573706F6E73651801200128095208726573706F6E736522410A0F4D756C7469706C6552657175657374122E0A0A6D756C74706C6552657118012003280B320E2E43726561746552657175657374520A6D756C74706C65526571224E0A0B53686F775265717565737412230A0D66756E6374696F6E5F6E616D65180120012809520C66756E6374696F6E4E616D65121A0A0876657273696F6E31180220012803520876657273696F6E3122260A0E53686F77416C6C5265717565737412140A0568656C6C6F180120012803520568656C6C6F22E2010A0C53686F77526573706F6E7365121C0A0966697273746E616D65180120012809520966697273746E616D65121A0A086C6173746E616D6518022001280952086C6173746E616D6512140A05656D61696C1803200128095205656D61696C121A0A0876657273696F6E31180420012803520876657273696F6E31121A0A086C616E677561676518052001280952086C616E677561676512230A0D66756E6374696F6E5F6E616D65180620012809520C66756E6374696F6E4E616D6512250A0866756E6374696F6E18072001280B32092E46756E6374696F6E520866756E6374696F6E22410A1453686F775265706561746564526573706F6E736512290A08726573706F6E736518012003280B320D2E53686F77526573706F6E73655208726573706F6E736522690A1353686F77437269746572696152657175657374121A0A086C616E677561676518012001280952086C616E6775616765121A0A0876657273696F6E31180220012803520876657273696F6E31121A0A086B6579776F72647318032001280952086B6579776F72647332A3020A05757365727312270A0A6164645F6E65775F666E120E2E437265617465526571756573741A092E526573706F6E736512260A076164645F666E7312102E4D756C7469706C65526571756573741A092E526573706F6E736512240A0964656C6574655F666E120C2E53686F77526571756573741A092E526573706F6E736512270A0773686F775F666E120C2E53686F77526571756573741A0E2E4372656174655265717565737412330A0C73686F775F616C6C5F666E73120C2E53686F77526571756573741A152E53686F775265706561746564526573706F6E736512450A1673686F775F616C6C5F776974685F637269746572696112142E53686F774372697465726961526571756573741A152E53686F775265706561746564526573706F6E7365620670726F746F33"};
}

